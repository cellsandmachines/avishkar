Large-scale, interactive MicroRNA Target Prediction
==============
Interactive, large-scale and accurate MicroRNA target prediction
using [Hadoop](http://hadoop.apache.org)
and [Spark](https://spark.apache.org/docs/latest/index.html). 
*The software has been tested on Spark 1.4. Newer versions of Spark might not be compatible with the software.*

Prerequisites
==============
The software released here depeneds on the following libraries that should be installed
prior to using the software.

* Python libraries:
    1. Python 2.7 or higher.
    2. [Numpy](http://www.numpy.org)
    3. [scikit-learn](http://scikit-learn.org)
    4. [Biopython](http://biopython.org/wiki/Main_Page)

* Third-party libraries:
    1. [RNAhybrid](http://bibiserv.techfak.uni-bielefeld.de/download/tools/rnahybrid.html)
    2. [RNAfold](http://www.tbi.univie.ac.at/RNA/RNAfold.html)

A working installation of Spark (*version 1.4*) is also required. Apache Hadoop is necessary to run the software
on a Hadoop cluster.

Quick Start
==============
* First change the config file *conf/avishkar.conf*
  to have the config keys *rna\_hybrid* and *rna\_fold* point
  to the correct location of the RNAhybrid and RNAfold binaries repectively.
* Then modify *conf/avishkar\_conf.sh*
  to have the *SPARK\_HOME* variable point to the Spark install location.
* In the following example we will start Avishkar in local mode (single machine) and compute the 10-fold 
  crossvalidation performance on the sample data provided.

        # Start the custom PySpark shell provided with the code.
        # Change executors/memory etc based on cluster size.
        ./bin/pyspark --master local

        # Or alternatively start the shell using IPython (recommended)
        IPYTHON=1 ./bin/pyspark --master local
        
        # First create an instance of AvishkarContext.
        > from avishkar import *
        > context = AvishkarContext(sc)  # sc is the SparkContext instance provided by Spark.
        
        # Create list of candidate target sets for the hsa-let-7 miRNA family.
        # and label them using CLIP-Seq data.
        > candidate_set_gen = CandidateSetGenerator(context)
        > candidate_set_rdd = candidate_set_gen.generate("hg18", "HUMAN_CLIP_LOCATIONS",
                                                         mirna_filter_func=lambda mi: mi.startswith("hsa-let-7")).cache()

        # Generate features for the candidate target sets
        > feature_gen = FeatureGenerator(context)
        > features_rdd = feature_gen.generate("hg18", candidate_set_rdd).cache()

        # Now lets print the number of data points in our feature set.
        > features_rdd.count()

        # Get performance metrics using 10-fold Crossvalidation
        > avishkar_app = AvishkarApp(context)
        > metrics = avishkar_app.crossvalidate(features_rdd)

        # Now lets print the FPR and TPR (False Positive/True Positive rates)
        metrics.get_fpr_tpr()


Training and Predicting on different mRNA-miRNA pairs.
======================================================

Avishkar provides features to get training data and for a set of mRNA-miRNA pairs
and predict on a different set of mRNA-miRNA pairs. Let's say we want to 
train our model on a set of 10 mRNAs and miR-19 miRNA family and predict on a
different set of 10 mRNAs and let-7 miRNA family.

    # Define the set of mRNAs we want to train on.
    > train_mrna_set = set([NM_001042681,NM_012102,NM_001042682,NM_017940,NM_181865,
                            NM_181864,NM_001033581,NM_199454,NM_015215,NM_002744])

    # Generate candidate set and label them using CLIP-seq data for humans.
    > train_candidate_set_rdd = candidate_set_gen.generate(
        "hg18", "HUMAN_CLIP_LOCATIONS",
         mrna_filter_func=lambda m: m in train_mrna_set,
         mirna_filter_func=lambda mi: mi.startswith("hsa-let-7")
      ).cache()
    
    # Get training features.
    > train_features = feature_gen.generate("hg18", train_candidate_set_rdd).cache()
    
    # Now define the set of mRNAs we want to predict for.
    > test_mrna_set = set([NM_001282162,NM_020765,NM_001206540,NM_012400,NM_001271814,
                           NR_033887,NM_032409,NM_001286239,NM_001286238,NM_001243767])

    # Generate test candidate sets. Note that we don't need to label the candidate sets
    # for the test data. However if we want to evaluate the prediction performance on the
    # test data then we will need to label the predictions generated later and then
    # compute the prediction performance.
    > test_candidate_set_rdd = candidate_set_gen.generate(
        "hg18",
         mrna_filter_func=lambda m: m in train_mrna_set,
         mirna_filter_func=lambda mi: mi.startswith("hsa-let-7")
      ).cache()
    # Now get the test set features
    > test_features = feature_gen.generate("hg18", test_candidate_set_rdd).cache()
    # Train the model
    avishkar_app = AvishkarApp(context)
    model = avishkar_app.train(train_features)
    # Now get predictions on test set
    predictions_rdd = avishkar_app.predict(model, test_features)
    # Now write the predictions to a file.
    predictions_rdd.map(lambda (r, l): "%d,%s" % (l, r.rid()))



Seed and Seedless sites
========================
Using Avishkar once can also get preditions specifically for seed (canonicanal seed matches)
or seedless (non-canonical seed matches) target sites. Lets say we have a Features RDD containing
both seed and seedless sites. In order to train and predict on seedless sites only we can do the following:
    
    # Lets turn on probability computation so that we can generate ROC curves.
    > context.conf.set_model_param('compute_prob', 'True')
    > model = avishkar_app.train(train_features, exclude_seed_sites=True)
    > predictions_rdd = avishkar_app.predict(model, test_features, exclude_seed_sites=True)
    # Lets plot the ROC curve for the predictions.
    > metrics = Metric(context)
    > metrics.fit(predictions_rdd)
    > fpr, tpr, thresholds = metrics.get_fpr_tpr()
    > import matplotlib.pyplot as plt
    > plt.plot(fpr, tpr)




Saving candidate sets and features
==================================
Generatting candidate set and the corresponding features for the candidate
set are expensive operations. So we might want to save them so that we can use
them later. Avishkar provides APIs for saving and loading both candidate sets
and features to and from CSV files.

* To save a candidate set rdd:

        # candidate_set_gen is the CandidateSetGenerator object
        > candidate_set_gen.write_to_file(candidate_set_rdd, '/tmp/candidate_set.csv')
        # Load candidate sets from file
        > candidate_set_rdd = candidate_set_gen.read_from_file('/tmp/candidate_set.csv')

* Similarly, features RDD can be saved to, and also loaded from, CSV files.

        # feature_gen is the FeatureGenerator object
        > feature_gen.write_to_file(features_rdd, '/tmp/features.csv')
        > features_rdd = feature_gen.read_from_file('/tmp/features.csv')



Running on Hadoop cluster
=========================
All the examples given above can be run on a Hadoop cluster without any change. 
An example invocation is given below:

        IPYTHON=1 ./bin/pyspark --master yarn-client \
            --num-executors 8 --driver-memory 6g \
            --executor-memory 7g --executor-cores 4 \
            --conf spark.akka.frameSize=30

The number of executors, driver memory etc should be cnaged based on the cluster size.
The only prerequisites to running on a Hadoop cluster are:

* The Avishkar code should be copied to the same location on all the nodes in the cluster and the configuration
  variable *AVISHKAR\_HOME* (in *conf/avishkar\_conf.sh*) should be changed to point to
  the location of Avishkar code.

* All the data files need to be uploaded to a location on HDFS.
  Additionally, the config file *conf/sparkdao.conf* should be modified to change
  the locations of the various data files to point to the correct location on HDFS.
  Absolute paths can be specifed by removing the `%(AVISHKAR_HOME)s` prefix from paths.
  Note that we have only provided samples of various data files needed to run our code.
  The complete data files can be easily obtained from various publicly available sources
  like e.g. the [UCSC Genome browser](http://genome.ucsc.edu).

* The third-party binaries RNAhybrid and RNAfold should be installed at the same location
  on each of the machine in the cluster and the config file *conf/avishkar.conf* should
  be updated accordingly.



Contact Information
===================
For questions and comments email <asish.geek@gmail.com>.

--------------------------------
This work was supported by NSF Center for Science of Information (CSoI)
Grant *CCF-0939370* and NSF Grants *IOS-1124962* and *CCF-1337158*