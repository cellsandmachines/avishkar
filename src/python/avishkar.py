# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys as _sys
import numpy as _np
import sklearn.cross_validation as _cv
import argparse as _argparse
import file_utils.batch_file_reader as _br
import spark.preprocess as _preprocess
import gc as _gc
import collections as _collections
import spark.train_predict as _spark
from pyspark import SparkContext
from spark.metrics import Metrics
from avishkar_context import AvishkarContext
from candidate_set import CandidateSetGenerator
from features import FeatureGenerator


# The first choice should be ALL
_REGION_CHOICES = set(['3UTR', '5UTR', 'CDS'])


def _parse_arguments():
    argparser = _argparse.ArgumentParser(
        description="Functional MiRNA Target Prediction"
    )

    subparsers = argparser.add_subparsers(
        title='Modes of operation', help='sub-command help.',
        dest='subparser_name'
    )
    # Parameters for Crosss Validation
    cv_parser = subparsers.add_parser(
        'crossvalidate', help='Compute Cross-Validation Performance'
    )
    cv_parser.add_argument(
        "--model-params",
        default=[20, 1.0, DEFAULT_REG_PARAM, 'LINEAR', 0.01],
        nargs='*',
        dest='model_params',
        help='model parameters: #basis: Defaults to (20), '
        'C: penalty for error term for kernel model (defaults to 1.0), '
        'reg_param: Regularization parameter for linear model'
        'kernel: (defaults to LINEAR), '
        'kernel_params: gamma for RBF (defaults to 0.01)'
    )
    cv_parser.add_argument("pos_file", metavar='pos_file',
                           help='path to positive data')
    cv_parser.add_argument("neg_file", metavar='neg_file',
                           help='path to negative data')
    cv_parser.add_argument(
        '--cv-runs', metavar='K', type=int, dest='cv_runs', default=10,
        help='K of the k-fold corssvalidation'
    )

    # Parameters for Tune
    tune_parser = subparsers.add_parser(
        'tune', help='Tune parameters using Cross-Validation '
        'Evaluate FN model at a grid of different parameter values.'
    )
    tune_parser.add_argument('--nbasis', dest='nbasis', default=[20], nargs='*',
                             help='Number of basis functions.')
    tune_parser.add_argument("--kernel", dest='kernel',
                             choices=['LINEAR', 'RBF'], default='LINEAR',
                             help='Type of kernel to use.')
    tune_parser.add_argument("--C", dest='C', nargs='*', default=[1.0],
                             help='The values of C for RBF kernel')
    tune_parser.add_argument("--gamma", dest='gamma', nargs='*', default=[0.01],
                             help='The values of gamma to use for RBF kernel')
    tune_parser.add_argument("--reg-param", dest='reg_param', nargs='*',
                             default=[DEFAULT_REG_PARAM],
                             help='Regularization parameters to use for '
                             'linear model')
    tune_parser.add_argument("pos_file", metavar='pos_file',
                             help='path to positive data')
    tune_parser.add_argument("neg_file", metavar='neg_file',
                             help='path to negative data')
    tune_parser.add_argument(
        '--cv-runs', metavar='K', type=int, dest='cv_runs', default=10,
        help='K of the k-fold corssvalidation'
    )

    # Parameters for Train/Predict
    predict_parser = subparsers.add_parser(
        'predict', help='Train and Predict on different datasets'
    )
    predict_parser.add_argument(
        "--model-params",
        default=[20, 1.0, DEFAULT_REG_PARAM, 'LINEAR', 0.01],
        nargs='*',
        dest='model_params',
        help='model parameters: #basis: Defaults to (20), '
        'C: penalty for error term for kernel model (defaults to 1.0), '
        'reg_param: Regularization parameter for linear model'
        'kernel: (defaults to LINEAR), '
        'kernel_params: gamma for RBF (defaults to 0.01)'
    )
    predict_parser.add_argument("pos_file", metavar='pos_train_file',
                                help='path to positive train data')
    predict_parser.add_argument("neg_file", metavar='neg_train_file',
                                help='path to negative train data')
    predict_parser.add_argument("pos_test_file", metavar='pos_test_file',
                                help='path to positive test data')
    predict_parser.add_argument("neg_test_file", metavar='neg_test_file',
                                help='path to negative test data')

    # Common options for all parsers
    argparser.add_argument(
        '--save', metavar='file', dest='save_fname', default=None,
        help='File to save the preprocessed data to'
    )
    argparser.add_argument("seed_enrichment_file",
                           metavar='seed_enrichment_file',
                           help='path to seed enrichment file')
    argparser.add_argument("output_file", metavar='output_file',
                           help='File to write the output to')

    argparser.add_argument(
        "--model", default='FN',
        choices=['FN', 'FN_PCA', 'MV'],
        help='type of model. FN (default): Functional therm and AU, '
        'FN_PCA: Functional features along with PCA '
        'MV: Multivariate features for all.'
    )
    argparser.add_argument(
        "--seed-sites", action='store_true',
        dest='seed_sites',
        help='Filter seed sites. By defaults only uses seedless sites'
    )
    argparser.add_argument(
        "--region", default='ALL', choices=list(_REGION_CHOICES) + ['ALL'],
        dest='region',
        help='Filter a given region. By default filters all region.'
    )
    argparser.add_argument(
        "--cluster-assignment", metavar='<file>',
        help='Path to CSV file that defines cluster assignment. '
        'The CSV file contains key value pairs where the key represents '
        'the key by which rows are assigned to clusters and  the value '
        'represents the cluster to which a row should be assigned. '
        'The values are converted to numeric cluster indices.',
        dest='cluster_assn_file', default=None
    )
    argparser.add_argument(
        "--train-iters", metavar='<iterations>', type=int,
        default=train_predict_spark.DEFAULT_ITERATIONS,
        dest="train_iters",
        help='# Training iterations.'
    )
    argparser.add_argument(
        "--mean-file", metavar='<mean file>', default=None, dest="mean_file",
        help='File containing mean curves which are used to replace NaN values'
    )
    argparser.add_argument(
        "--infer-test-mean", action='store_true', dest="infer_test_mean",
        help='Infer the mean to use for test points rather than directly '
        'using the appropriate mean based on Test Label'
    )
    argparser.add_argument(
        '--dataset', default='',
        help='Used to write to dataset specific DFS paths'
    )
    argparser.add_argument(
        '--save-train-data',
        dest='save_train_data', action='store_true',
        help='Turns on whether or not to save transformed training/test data'
    )
    argparser.add_argument(
        '--cache-pca', metavar='path', dest='pca_cache_path', default=None,
        help='Path where to cache PCA objs to.'
        ' If not provided PCA obj are recomputed everytime during training.'
    )
    argparser.add_argument(
        '--subsample-runs', type=int, dest='subsample_runs', default=10,
        help='Number of times to subsample from the larger dataset'
    )
    argparser.add_argument(
        '--sample-frac', dest='sample_frac', default=1.0,
        help='The amount of positive data to subsample. Defaults to 1.0 '
        'which doesnt subsample positive data'
    )
    args = argparser.parse_args()
    # Print arguments
    print "Arguments:"
    for k, v in vars(args).iteritems():
        print k, " => ", v

    return args


def _sample_rows(features, indices):
    # Returns rows from @features present in @indices
    return features.zipWithIndex()\
        .filter(lambda (r, i): True if i in indices else False)\
        .map(lambda x: x[0])


def _combine_pos_neg_data(pos_data, neg_data):
    partitions = pos_data.getNumPartitions()
    combined_data = pos_data.union(neg_data)
    combined_data = combined_data.coalesce(partitions)
    return combined_data


def _train_predict_wrapper(avishkar_context,
                          pos_train_data, neg_train_data,
                          pos_test_data, neg_test_data,
                          cluster_assignment=None,
                          curve_means=None, infer_test_mean=False,
                          ):

    train_data = _combine_pos_neg_data(pos_train_data, neg_train_data)
    test_data = _combine_pos_neg_data(pos_test_data, neg_test_data)
    model = _spark.train(avishkar_context, train_data, cluster_assignment=cluster_assignment,
        curve_means=curve_means)

    return _spark.predict(
        avishkar_context, model,
        test_data,
        cluster_assignment=cluster_assignment,
        curve_means=curve_means,
        infer_test_mean=infer_test_mean
    )


def _get_tune_params(args):
    tune_params = _collections.OrderedDict([])
    tune_params['nbasis'] = map(int, args.nbasis)
    tune_params['C'] = map(float, args.C)
    tune_params['reg_param'] = map(float, args.reg_param)
    tune_params['gamma'] = map(float, args.gamma)
    return tune_params


def _get_model_params(model, param_list):
    model_params = {
        'basis': int(param_list[0]),
        'C': float(param_list[1]),
        'reg_param': float(param_list[2]),
        'kernel': param_list[3],
        'kernel_params': {},
        'prob': True,
    }
    # model takes the following vals: 'FN', 'FN_PCA', 'MV'
    print "Model type:", model
    if model == 'FN_PCA':
        raise Exception('Model FN_PCA not implemented currently.')
    elif model == 'MV':
        model_params['basis'] = -1

    if not (param_list[3] == 'LINEAR' or param_list[3] == 'RBF'):
        raise Exception('Only LINEAR and RBF kernels are supported')

    if param_list[3] == 'RBF':
        if len(param_list) >= 5 and param_list[4] != 'None':
            gamma = float(param_list[4])
            model_params['kernel_params']['gamma'] = gamma

    print "Model params:"
    for k, v in sorted(model_params.iteritems(), key=lambda (x, y): x):
        print k, "=>", v

    return model_params


def _print_tune_results(ofp, results, header=[]):
    # Print the header
    ofp.write("%s,Accuracy,Recall,TPR,FPR,Misclassification\n" %
              ",".join(header))
    for key in results.keys():
        params = ",".join([str(x) for x in key])
        for conf_matrix in results[key]:
            acc, recall, fpr, tpr, error = get_metrics(conf_matrix)
            ofp.write("%s,%.3f,%.3f,%.3f,%.3f,%.3f\n" %
                      (params, acc, recall, tpr, fpr, error))


def _subsample_data(pos_data, neg_data, num_pos, num_neg, seed,
                   sample_frac=1.0):
    pos_data_sample = pos_data
    neg_data_sample = neg_data

    # Compute sample size with respect to the smaller dataset
    if num_pos > num_neg:
        sample_size = sample_frac * num_neg
    else:
        sample_size = sample_frac * num_pos

    pos_data_sample = pos_data.sample(False, sample_size/num_pos, seed)
    neg_data_sample = neg_data.sample(False, sample_size/num_neg, seed)

    return pos_data_sample, neg_data_sample


def _read_cluster_assignment(fname):
    if fname is None:
        return None
    cluster_assignment = []
    reader = _br.BatchFileReader(fname, process_row=lambda x: x.strip().split(","))
    reader.read(cluster_assignment)

    distinct_vals = set([v for k, v in cluster_assignment])
    cluster_id_map = dict([(k, i) for i, k in enumerate(sorted(distinct_vals))])
    print "Cluster IDS:"
    for key in sorted(cluster_id_map.keys()):
        print "%s -> %d" % (key, cluster_id_map[key])

    cluster_assignment_map = {}
    print "Cluster Assignment:"
    for key, val in cluster_assignment:
        cluster_assignment_map[key] = cluster_id_map[val]
        print "%s -> %d" % (key, cluster_assignment_map[key])
    return cluster_assignment_map


def _read_mean_curves(fname):
    temp = []
    curve_means = []
    reader = _br.BatchFileReader(fname)
    reader.read(temp)
    curve_means = [None]*(len(temp)/2)
    for i in range(len(temp)/2):
        curve_means[i] = (_preprocess.str_to_array(temp[2*i]),
                          _preprocess.str_to_array(temp[2*i + 1]))
    del temp
    return curve_means


class AvishkarApp:
    def __init__(self, avishkar_context,
                 seed_enrichment_table_name="HUMAN_SEED_ENRICHMENT"):
        """
            @avishkar_context: AvishkarContext object
            The table to use to read seed enrichment scores. By default
            uses the table HUMAN_SEED_ENRICHMENT to map seed alignment patterns
            to enrichment scores.
        """
        self._context = avishkar_context
        self._seed_enrichment_tname = seed_enrichment_table_name
        # TODO: Fix this
        self._curve_means = None
        self._cluster_assignment = None
        self._initialize()

    def _initialize(self):
        # set numpy random seed so that we have deterministic behavior acorss
        # runs
        _np.random.seed(self._context.conf.get_random_seed())
        self._generate_new_random_seed()

    def _generate_new_random_seed(self):
        self._rand_seed = _np.random.randint(0, 10000)

    def _preprocess_data(self, data_rdd):
        seed_enrichment_map = self._context.dao.get_seed_enrichment_map(
            self._seed_enrichment_tname
        )
        data_rdd = _preprocess.preprocess_data(
            data_rdd, seed_enrichment_map=seed_enrichment_map)
        partitions = self._context.conf.num_partitions()
        data_rdd = data_rdd.repartition(partitions)
        return data_rdd

    def _read_subsampled_data(self, rdd):
        pos_data = rdd.filter(lambda x: x.label() == 1)
        neg_data = rdd.filter(lambda x: x.label() == 0)
        num_pos = pos_data.count()
        num_neg = neg_data.count()

        pos_sampled_data, neg_sampled_data = _subsample_data(
            pos_data, neg_data, num_pos, num_neg, self._rand_seed,
            sample_frac=self._context.conf.sampling_sample_frac())

        return pos_sampled_data, neg_sampled_data

    def crossvalidate(self, rdd):
        cv_k = self._context.conf.crossvalidation_folds()
        print "Performing %d-fold crossvalidation" % cv_k
        rdd = self._preprocess_data(rdd)
        pos_data, neg_data = self._read_subsampled_data(rdd)

        len_pos = pos_data.count()
        len_neg = neg_data.count()

        pos_kf = _cv.KFold(len_pos, cv_k)
        neg_kf = _cv.KFold(len_neg, cv_k)
        pos_ind = [(train, test) for train, test in pos_kf]
        neg_ind = [(train, test) for train, test in neg_kf]

        metrics = Metrics(self._context)

        for ind in zip(pos_ind, neg_ind):
            pos_train_ind = ind[0][0]
            pos_test_ind = ind[0][1]
            neg_train_ind = ind[1][0]
            neg_test_ind = ind[1][1]

            pos_train_data = _sample_rows(pos_data, pos_train_ind)
            pos_test_data = _sample_rows(pos_data, pos_test_ind)
            neg_train_data = _sample_rows(neg_data, neg_train_ind)
            neg_test_data = _sample_rows(neg_data, neg_test_ind)

            preds_iter = _train_predict_wrapper(
                self._context,
                pos_train_data, neg_train_data,
                pos_test_data, neg_test_data,
                cluster_assignment=self._cluster_assignment
            )
            metrics_iter = Metrics(self._context)
            metrics_iter.fit(preds_iter)
            # Print training metrics
            print "Test error:", metrics_iter.misclassification_error()
            metrics += metrics_iter
        return metrics

    def train(self, train_data, subsample=True, **exclude_args):
        """
        Train an SVM model.
        :param train_data: Training data RDD
        :param subsample: If True (default) then subsample to extract equal number of +ve/-ve examples.
        :param exclude_args: Can be the following: exclude_seed_sites, exclude_seedless_sites, exclude_region
        exclude_seed_sites, exclude_seedless_sites are False by default while exclude_region is the empty set.
        exclude_region parameter can be a set containg any of the following three: 5UTR, 3UTR, CDS.
        :return: Model
        """
        train_data = self._preprocess_data(train_data, **exclude_args)
        if subsample:
            pos_train_data, neg_train_data = self._read_subsampled_data(train_data)
            train_data = _combine_pos_neg_data(pos_train_data, neg_train_data)

        return _spark.train(self._context, train_data,
                            cluster_assignment=self._cluster_assignment,
                            curve_means=self._curve_means)

    def predict(self, model, test_data, **exclude_args):
        """
        Predict on test data given a model.
        :param model: Model
        :param test_data: Test data (RDD)
        :param exclude_args: Can be the following: exclude_seed_sites, exclude_seedless_sites, exclude_region
        exclude_seed_sites, exclude_seedless_sites are False by default while exclude_region is the empty set.
        exclude_region parameter can be a set containg any of the following three: 5UTR, 3UTR, CDS.
        :return: Predictions RDD which is a tuple of (Record object, label) or (Record object, prob)
        depending on if compute_prob paramter is False of True.
        """
        test_data = self._preprocess_data(test_data, **exclude_args)
        return _spark.predict(self._context, model, test_data,
                       cluster_assignment=self._cluster_assignment,
                       curve_means=self._curve_means)

    def subsample(self, data):
        """
            Subsample data to have equal number of positive and negative
            examples.
        """
        pos_data, neg_data = self._read_subsampled_data(data)
        return _combine_pos_neg_data(pos_data, neg_data)

    # TODO: Fix this.
    """
    def _tune(self):
        results = _collections.OrderedDict()
        for p in itertools.product(*self._tune_params.values()):
            self._model_params = _get_model_params(
                'FN', [p[0], p[1], p[2], self._args.kernel, p[3]]
            )
            # We don't want to compute probabilities during CV.
            self._model_params['prob'] = False
            conf_matrices = self.crossvalidate()
            results[p] = conf_matrices
        return results
    """

    def run(self):
        # TODO: Fix this
        pass
        """
        if self._args.subparser_name == 'predict':
            action = self._train_predict
            process_results = print_results
        elif self._args.subparser_name == 'crossvalidate':
            action = self.crossvalidate
            process_results = print_results
        elif self._args.subparser_name == 'tune':
            action = self._tune
            process_results = partial(_print_tune_results,
                                      header=self._tune_params.keys())

        t1 = time.time()
        for i in range(self._args.subsample_runs):
            self._generate_new_random_seed()
            results = action()
            process_results(self._ofp, results)
        t2 = time.time()
        print "Total time: %.3f minutes" % ((t2 - t1)/60.0)
        """


def _main():
    _gc.set_debug(_gc.DEBUG_STATS)
    args = _parse_arguments()
    sc = SparkContext(appName="Avishkar")
    app = AvishkarApp(sc, args)
    app.run()


if __name__ == '__main__':
    _sys.exit(_main())
