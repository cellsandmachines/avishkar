# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class LocationWithAttribute:
    def __init__(self, loc, energy, seed_type):
        self._loc = loc
        self._energy = energy
        self._seed_type = seed_type

    def __eq__(self, other):
        return self._loc == other._loc

    def __ne__(self, other):
        return self._loc != other._loc

    def __hash__(self):
        return hash(self._loc)

    def to_str(self):
        seed_type =\
            "None" if self._seed_type is None else ("%d-%d" % self._seed_type)
        return "%d-%d,%.3f,%s" % (
            self._loc[0], self._loc[1], self._energy, seed_type
        )

    def __str__(self):
        return self.to_str()

    @staticmethod
    def from_str(str_obj):
        tokens = str_obj.strip().split(",")
        (loc, energy, seed_type) = tokens
        loc = tuple(map(int, loc.split(",")))
        energy = float(energy)
        seed_type =\
            None if seed_type == "None" else tuple(map(int, loc.split(",")))
        return LocationWithAttribute(loc, energy, seed_type)

    def is_preferable(self, other):
        # Returns True if self is prefferable over @other LocationWithAttribute
        # object
        # Seed site is preferred over seed-less site
        # A seed-match of greater size is preferred
        # A site with lower energy is preferred.
        if self._seed_type is None:
            if other._seed_type is None:
                # Check energy
                if self._energy < other._energy:
                    return True
                else:
                    return False
            else:
                return False
        else:
            if other._seed_type is None:
                return True
            else:
                # Check size of seed match
                if (self._seed_type[1] - self._seed_type[0]) >\
                        (other._seed_type[1] - other._seed_type[0]):
                    return True
                else:
                    return False

        if other._seed_type is None and self._seed_type is not None:
            return True
        if other._seed_type is not None and \
                self._seed_type is not None and \
                ((self._loc[1] - self._loc[0]) >
                 (other._loc[1] - other._loc[0])):
            return True
        if self._energy < other._energy:
            return True
        return False
