# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
    Encapsulates a record containing a label, row_id and feature list
"""
import numpy as np


class Record:
    def __init__(self, feature_list, label=None, record_id=None):
        self._feature_list = feature_list
        self._label = label
        self._record_id = record_id

    def __getitem__(self, index):
        return self._feature_list[index]

    def __len__(self):
        return len(self._feature_list)

    def __setitem__(self, key, value):
        self._feature_list[key] = value

    def __delitem__(self, index):
        del self._feature_list[index]

    def __iter__(self):
        return iter(self._feature_list)

    def __array__(self):
        return np.array(self._feature_list)

    def __eq__(self, other):
        return self._record_id == other.rid()

    def __hash__(self):
        return hash(self._record_id)

    def label(self):
        return self._label

    def rid(self):
        return self._record_id


class LabeledDataPoint:
    def __init__(self, features, label=None, record_id=None):
        self.label = label
        self.rid = record_id
        self.features = features
