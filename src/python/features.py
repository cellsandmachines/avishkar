# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


'''
    Input: File with mrna-mirna clip data.
    Outputs the following features:
        a) With window length ~22 and centered around the entire target region
            1) del G curve
            2) del del G curve
            3) AU composition curve
            4) mean conservation curve
        b) For window length =8 and centered around seed match region
            1) del G curve
            2) del del G curve
            3) AU composition curve
            5) mean conservation curve
        c) Region in which the site is present: 3UTR, 5UTR, CDS
        d) Location of target from the end of the region (range: 0 to 1)
        e) Enrichment of seed type for both seed and seedless sites.
        f) Size of target site

    Possible future features:
        1) Number of seed matches withing the seed region.
        2) Avg binding of the top N alignments within the site.
'''
import traceback
import argparse
import sys
import numpy as np
import re
from third_party.rnahybrid import RNAHybrid
from third_party.rnafold import RNAFold
from models.record import Record


MATCH, MISMATCH, GAP, WOBBLE = 1, 2, 3, 4
WINDOWS = 13  # Number of windows
WINDOW_LEN_SITE = 46  # TODO
WINDOW_LEN_SEED = 9
SEED_TYPES = [(1, 8), (1, 7), (2, 8), (2, 7), (1, 6), (3, 8), None]


def get_align_site(output):
    # parse RNA hybrid output and get the site within the mRNA where
    # the miRNA is aligned to.
    output = output.strip()
    output = output.split(":")

    if not output or not output[0]:
        return None

    mrna_mismatch, mrna_match, mirna_match, mirna_mismatch =\
        output[7], output[8], output[9], output[10]

    max_align_len = [mrna_mismatch, mrna_match, mirna_mismatch, mirna_match]
    max_align_len = max(map(lambda x: len(x), max_align_len))
    mrna_mismatch += " " * (max_align_len - len(mrna_mismatch))
    mrna_match += " " * (max_align_len - len(mrna_match))
    mirna_mismatch += " " * (max_align_len - len(mirna_mismatch))
    mirna_match += " " * (max_align_len - len(mirna_match))

    mrna_align_len = 0
    for i in range(max_align_len):
        # If both mismatch and match are not gaps then increment
        # the mrna align length.
        if mrna_mismatch[i] == " " and mrna_match[i] == " ":
            continue
        mrna_align_len += 1

    site_start = int(output[6])
    site_end = site_start + mrna_align_len
    return (site_start, site_end)


def get_seed_match_type(mrna_mismatch, mrna_match,
                        mirna_mismatch, mirna_match):
    # Returns the alignment of the first 8 (from the 5' end)
    # nucleotide positions of the miRNA

    max_align_len = [mrna_mismatch, mrna_match, mirna_mismatch, mirna_match]
    max_align_len = max(map(lambda x: len(x), max_align_len))
    mrna_mismatch += " " * (max_align_len - len(mrna_mismatch))
    mrna_match += " " * (max_align_len - len(mrna_match))
    mirna_mismatch += " " * (max_align_len - len(mirna_mismatch))
    mirna_match += " " * (max_align_len - len(mirna_match))

    mirna_start_offset = 0  # accounts for gaps at the beginning of miRNA.
    for i in range(1, max_align_len):
        if mirna_mismatch[-i] != " " or mirna_match[-i] != " ":
            break
        mirna_start_offset += 1

    mirna_aligned_chars = 0
    alignment = []

    for i in range(1 + mirna_start_offset, max_align_len):
        if mirna_match[-i] == ' ':
            if mirna_mismatch[-i] != ' ' and mrna_mismatch[-i] != ' ':
                alignment.append(MISMATCH)
            else:
                alignment.append(GAP)
        elif (mirna_match[-i] == 'G' and mrna_match[-i] == 'U') or\
             (mrna_match[-i] == 'G' and mirna_match[-i] == 'U'):
            alignment.append(WOBBLE)
        else:
            alignment.append(MATCH)

        if mirna_match[-i] != ' ' or mirna_mismatch[-i] != ' ':
            mirna_aligned_chars += 1

        if mirna_aligned_chars >= 8:
            break

    alignment = array_to_str(alignment[::-1], delimiter="")
    return alignment


def parse_rna_hybrid_output(output, seed_type=False):
    # Parse output to get the following features:
    #   Mean free energy (mfe) of duplex.
    #   alignment of the seed region of miRNA if @seed_type is True

    output = output.strip()
    output = output.split(":")
    if not output or not output[0]:
        return None
    mfe = float(output[4])
    if not mfe < 0.0:
        return None

    if seed_type is False:
        return mfe

    mrna_mismatch, mrna_match, mirna_match, mirna_mismatch =\
        output[7], output[8], output[9], output[10]
    seed_match_type = get_seed_match_type(
        mrna_mismatch, mrna_match, mirna_mismatch, mirna_match
    )

    return (mfe, seed_match_type)


def compute_delg_window(rna_hybrid, mrna_seq, mirna_seq, seed_type=None):
    delg = 0.0
    if seed_type is not None:
        output = rna_hybrid(mrna_seq, mirna_seq,
                            helix_constraint=seed_type)
        parsed_out = parse_rna_hybrid_output(output)
        if parsed_out is not None:
            delg = parsed_out
        return delg

    for seed_type in SEED_TYPES:
        output = rna_hybrid(mrna_seq, mirna_seq,
                            helix_constraint=seed_type)
        parsed_out = parse_rna_hybrid_output(output)
        if parsed_out is not None:
            delg = parsed_out
            break
    return delg


def compute_delg(rna_hybrid, mrna_seq, mirna_seq,
                 site_loc, site_seed_type, window_len):
    mrna_len = len(mrna_seq)
    delg = np.repeat(np.nan, WINDOWS*2 + 1)
    delg[WINDOWS] = compute_delg_window(rna_hybrid,
                                        mrna_seq[site_loc[0]:site_loc[1]],
                                        mirna_seq,
                                        site_seed_type)
    for i in range(WINDOWS):
        start = site_loc[1] + (i * window_len)
        end = min(mrna_len, start + window_len)
        delg[WINDOWS + i + 1] = compute_delg_window(
            rna_hybrid,
            mrna_seq[start:end],
            mirna_seq
        )
        if end == mrna_len:
            break

    for i in range(WINDOWS):
        end = site_loc[0] - (i * window_len)
        start = max(0, end - window_len)
        delg[WINDOWS - i - 1] = compute_delg_window(
            rna_hybrid,
            mrna_seq[start:end],
            mirna_seq
        )
        if start == 0:
            break
    return delg


def parse_rna_fold_output(output):
    mfe_open = 0.0
    output = output.split('\n')
    if len(output) < 2:
        print output
        raise Exception("Couldn't parse output")

    m = re.search(r'\(\s*([-0-9]*[.]{0,1}[0-9]+)\)$', output[1])
    if m:
        mfe_open = float(m.group(1))
    else:
        print output
        raise Exception("Couldn't parse output")
    return mfe_open


def get_seed_match_site(rna_hybrid, mrna_seq, mirna_seq, site_loc, seed_type):
    '''
        Returns the seed match site given a mrna and mirna. If the
        seed_type is not None then returns the seed match site in the
        mRNA by searching for seed seq (reverse comp)
        from the end of the mRNA site.
        If the seed_type is None then returns the location of the best
        possible hybridization of nucleotides 1-8 of mirna with mrna
        Note: The seed_site interval returned is a open on the right.
    '''
    if seed_type is not None:
        # Just do a string search to find the @seed_type within @site_loc
        seed_seq = mirna_seq[seed_type[0] - 1: seed_type[1]]
        seed_seq = str(seed_seq.reverse_complement())
        mrna_site_seq = str(mrna_seq[site_loc[0]: site_loc[1]])
        pos = [m.start() for m in re.finditer(seed_seq, mrna_site_seq)]

        if len(pos) == 0:
            raise Exception("Could not find seed match"
                            "mRNA seq: %s \n"
                            "miRNA seed seq: %s" % (mrna_site_seq, seed_seq))

        seed_site = (pos[-1] + site_loc[0],
                     pos[-1] + site_loc[0] + seed_type[1] - seed_type[0] + 1)
    else:
        seed_seq = mirna_seq[:8]  # find the best match
        output = rna_hybrid(mrna_seq[site_loc[0]:site_loc[1]],
                            seed_seq)
        seed_site = get_align_site(output)
        seed_site = (seed_site[0] + site_loc[0],
                     seed_site[1] + site_loc[0])
        if seed_site is None:
            raise Exception('Could not align miRNA seed seq')
    return seed_site


def compute_curve(func, mrna_seq, site_loc, window_len, **func_kw_args):
    mrna_len = len(mrna_seq)
    data = np.repeat(np.nan, WINDOWS*2 + 1)
    data[WINDOWS] = func(
        mrna_seq[site_loc[0]:site_loc[1]], **func_kw_args
    )
    for i in range(WINDOWS):
        start = site_loc[1] + (i * window_len)
        end = min(mrna_len, start + window_len)
        data[WINDOWS + i + 1] = func(
            mrna_seq[start:end], **func_kw_args
        )
        if end == mrna_len:
            break

    for i in range(WINDOWS):
        end = site_loc[0] - (i * window_len)
        start = max(0, end - window_len)
        data[WINDOWS - i - 1] = func(
            mrna_seq[start:end], **func_kw_args
        )
        if start == 0:
            break
    return data


def compute_mfe_open_window(mrna_seq, rna_fold=None):
    output = rna_fold(mrna_seq)
    try:
        parsed_output = parse_rna_fold_output(output)
    except:
        print "Caught exception while processing mRNA seq:", mrna_seq
        raise
    return parsed_output


def compute_deldelg(rna_fold, mrna_seq, delg, site_loc, window_len):
    mfe_open = compute_curve(compute_mfe_open_window, mrna_seq,
                             site_loc, window_len, rna_fold=rna_fold)
    deldelg = delg - mfe_open
    return deldelg


def compute_au_window(mrna_seq):
    au_content = [1 if x == 'A' or x == 'U' else 0 for x in mrna_seq]
    au_content = sum(au_content)
    return float(au_content)/float(len(mrna_seq))


def compute_au(mrna_seq, site_loc, window_len):
    return compute_curve(compute_au_window, mrna_seq, site_loc, window_len)


def compute_consv(consv, mrna, site_loc, seed_type, seed_site):
    seed_consv = np.nan
    offseed_consv = np.nan
    consv = consv[site_loc[0]:site_loc[1]]

    if seed_type is not None:
        site_len = site_loc[1] - site_loc[0]
        seed_indices = np.repeat(False, site_len)
        seed_indices[seed_site[0] - site_loc[0]:
                     seed_site[1] - site_loc[0]] = True
        seed_consv = np.mean(consv[seed_indices])
        offseed_consv = np.mean(consv[~seed_indices])

    consv = np.mean(consv)
    return (consv, seed_consv, offseed_consv)


# Returns true if smaller location (@loc)
# is contained within the larger location (@other_loc)
def contains(loc, other_loc):
    ''' Returns True of @loc is contained within @other_loc '''
    if loc[0] >= other_loc[0] and loc[1] <= other_loc[1]:
        return True
    return False


# Returns the percentage (wrt smaller interval) of overlap between 2 intervals.
def overlap_perct(interval, other_interval):
    interval_size = min(interval[1] - interval[0],
                        other_interval[1] - interval[0])
    overlap_size = min(interval[1], other_interval[1]) -\
        max(interval[0], other_interval[0])
    overlap_size = max(0, overlap_size)
    return float(overlap_size)/interval_size


def compute_misc_features(utr5_range, utr3_range, cds_range,
                          mrna, mirna, site_loc, seed_type,
                          rna_hybrid=None):
    site_len = site_loc[1] - site_loc[0]

    print "mRNA name:", mrna._name
    print "utr5: ", utr5_range
    print "utr3: ", utr3_range
    print "cds: ", cds_range

    if overlap_perct(site_loc, utr5_range) >= 0.95:
        site_region_str, site_region = "5UTR", utr5_range
    elif overlap_perct(site_loc, utr3_range) >= 0.95:
        site_region_str, site_region = "3UTR", utr3_range
    elif overlap_perct(site_loc, cds_range) >= 0.96:
        site_region_str, site_region = "CDS", cds_range
    else:
        site_region = "UNKNOWN"

    # get the relative location of the site within the region
    rel_site_loc = float(site_loc[0] - site_region[0]) / \
        (site_region[1] - site_region[0] + 1)
    rel_site_loc = max(0, rel_site_loc)  # make 0 if negative
    rel_site_loc = min(1, rel_site_loc)  # make 1 if > 1

    # get alignment
    output = rna_hybrid(mrna._seq[site_loc[0]: site_loc[1]],
                        mirna._seq, helix_constraint=seed_type)
    _, alignment = parse_rna_hybrid_output(output, seed_type=True)
    alignment = "".join([str(x) for x in alignment])
    return (site_region_str, rel_site_loc, alignment, site_len)


def compute_features(mrna, mirna, candidate_sets, features,
                     rna_hybrid=None,
                     rna_fold=None):

    mirna_seed_seq = mirna._seq[:8]
    (utr5_range, utr3_range, cds_range, consv_seq) = features
    feature_records = []

    for (label, loc_attr) in candidate_sets:
        try:
            seed_site = get_seed_match_site(
                rna_hybrid, mrna._seq, mirna._seq,
                loc_attr._loc, loc_attr._seed_type
            )
            delg_site = compute_delg(rna_hybrid, mrna._seq, mirna._seq,
                                    loc_attr._loc, loc_attr._seed_type,
                                    WINDOW_LEN_SITE)

            delg_seed = compute_delg(rna_hybrid, mrna._seq, mirna_seed_seq,
                                    seed_site, loc_attr._seed_type,
                                    WINDOW_LEN_SEED)

            deldelg_site = compute_deldelg(rna_fold, mrna._seq, delg_site,
                                        loc_attr._loc,
                                        WINDOW_LEN_SITE)

            deldelg_seed = compute_deldelg(rna_fold, mrna._seq, delg_seed,
                                        seed_site,
                                        WINDOW_LEN_SEED)

            au_site = compute_au(mrna._seq, loc_attr._loc, WINDOW_LEN_SITE)
            au_seed = compute_au(mrna._seq, seed_site, WINDOW_LEN_SEED)

            # consv is a tuple of 3 elements:
            # site consv, seed consv, offseed consv
            consv = compute_consv(consv_seq, mrna, loc_attr._loc,
                                  loc_attr._seed_type, seed_site)

            misc_features = compute_misc_features(
                utr5_range, utr3_range, cds_range,
                mrna, mirna,
                loc_attr._loc,
                loc_attr._seed_type,
                rna_hybrid=rna_hybrid
            )

            features = dict(delg_site=delg_site, delg_seed=delg_seed,
                            deldelg_site=deldelg_site, deldelg_seed=deldelg_seed,
                            au_site=au_site, au_seed=au_seed,
                            consv_site=consv[0], consv_seed=consv[1], consv_offseed=consv[2],
                            site_region=misc_features[0], rel_site_loc=misc_features[1],
                            seed_alignment=misc_features[2], site_len=misc_features[3])

            feature_record = Record(
                features, label=label,
                record_id=(mrna._name, mirna._name, loc_attr._loc)
            )
            feature_records.append(feature_record)

        except:
            # Ignore exceptions and move on
            traceback.print_exc()
            exp_string = "mRNA: %s" % mrna._name
            exp_string += " miRNA: %s" % mirna._name
            exp_string += " target loc: %s" % str(loc_attr._loc)
            exp_string += " seed_type: %s" % str(loc_attr._seed_type)
            print "Error while processing %s." % exp_string

    return feature_records


def array_to_str(arr, delimiter=" "):
    return delimiter.join([str(x) for x in arr])


def feature_to_csv(features):
    (mrna_name, mirna_name, site) = features.rid()
    site = (site[0] + 1, site[1])  # 1 based, closed interval
    label = features.label()
    label = str(label) if label is not None else "None"
    csv_str = "%s,%s,%s,%s,%s," % (
        label,
        mrna_name,  # mrna name
        mirna_name,  # mirna name
        array_to_str(site, delimiter="-"),  # site
        array_to_str(site, delimiter="-"),  # TODO: Remove this for release.
    )
    csv_str += "%s,%s,%s,%s,%s,%s," % (
        array_to_str(features['delg_site']),  # degG site
        array_to_str(features['delg_seed']),  # delG seed
        array_to_str(features['deldelg_site']),  # deldelG site
        array_to_str(features['deldelg_seed']),  # deldelG seed
        array_to_str(features['au_site']),  # au site
        array_to_str(features['au_seed']),  # au seed
    )
    # write conservation
    csv_str += "%f,%f,%f," % (features['consv_site'],  # consv site
                              features['consv_seed'],  # consv seed
                              features['consv_offseed'])  # consv offseed

    # write misc features
    csv_str += "%s,%f,%s,%d" % (
        features['site_region'],  # site region
        features['rel_site_loc'],  # rel site loc
        features['seed_alignment'], # seed alignment
        features['site_len']  # site len
    )
    return csv_str


def parse_str_array(str_arr, delimiter=" ", conv_func=float):
    return np.array([conv_func(x) for x in str_arr.split(delimiter)])


def csv_to_feature(row):
    # Exracts columns from a single row.
    row = row.strip().split(",")
    label = float(row[0])
    mrna_name = row[1]
    mirna_name = row[2]
    site_loc = row[3]
    site_loc = map(int, site_loc.split("-"))
    site_loc[0] -= 1 # Zero based. End not included.
    row_id = (mrna_name, mirna_name, site_loc)

    # TODO: This is 1:5 for now to read existing files from HDFS that had
    # the old format. Change this to 1:4 for release.
    row = row[5:]

    delg_site = parse_str_array(row[0])
    delg_seed = parse_str_array(row[1])
    deldelg_site = parse_str_array(row[2])
    deldelg_seed = parse_str_array(row[3])
    au_site = parse_str_array(row[4])
    au_seed = parse_str_array(row[5])

    consv = map(float, row[6:9])
    site_region = row[9]
    rel_site_loc = float(row[10])
    seed_alignment = row[11]
    site_len = int(row[12])

    features = dict(delg_site=delg_site, delg_seed=delg_seed,
                    deldelg_site=deldelg_site, deldelg_seed=deldelg_seed,
                    au_site=au_site, au_seed=au_seed,
                    consv_site=consv[0], consv_seed=consv[1], consv_offseed=consv[2],
                    site_region=site_region, rel_site_loc=rel_site_loc,
                    seed_alignment=seed_alignment, site_len=site_len)

    return Record(features, label=label, record_id=row_id)


class FeatureGenerator(object):
    def __init__(self, avishkar_context):

        self._context = avishkar_context
        self._rna_hybrid = RNAHybrid(self._context.conf)
        self._rna_fold = RNAFold(self._context.conf)
        self._num_partitions = self._context.dao.default_partitions
        self._partition_func = self._context.dao.partition_func

    def generate(self, assembly, candidate_set_rdd, mrna_filter_func=None):
        features_rdd = self._context.dao\
            .get_mrna_features(assembly, mrna_filter_func=mrna_filter_func)

        rdd1 = candidate_set_rdd.map(lambda r: (r[0]._name, r))
        num_partitions = self._num_partitions
        partition_func = self._partition_func

        rdd3 = rdd1.join(features_rdd)\
            .partitionBy(num_partitions, partition_func)

        rna_hybrid = self._rna_hybrid
        rna_fold = self._rna_fold
        return rdd3.flatMap(
            lambda (_, (c, f)): compute_features(c[0], c[1], c[2], f,
                                                 rna_hybrid=rna_hybrid,
                                                 rna_fold=rna_fold)
        )

    def write_to_file(self, features_rdd, file_name):
        """
            Writes the features RDD (@features_rdd) to the HDFS file
            @file_name
        """
        features_rdd.map(feature_to_csv).saveAsTextFile(file_name)

    def read_from_file(self, file_name):
        """
            Returns an RDD of feature records given a HDFS file.
        """
        sc = self._context.sc
        rdd = sc.textFile(file_name).map(csv_to_feature)
        return rdd


def parse_arguments():
    argparser = argparse.ArgumentParser(
        description="Generate features for miRNA target prediction."
    )
    argparser.add_argument(
        "assembly", choices=['mm9', 'hg19', 'hg18'],
        help="The sequence assembly to use for mRNA."
    )
    argparser.add_argument(
        "candidate-set-path", dest='candidate_set_path',
        help="Path to the file containing candidate set data."
    )
    argparser.add_argument(
        "output_file",
        help="File to write the output to."
    )
    args = argparser.parse_args()
    return args


def main():
    pass
    # TODO: Fix this
    """
    args = parse_arguments()
    sc = SparkContext(appName="Avishkar:: Feature generation")
    avishkar_conf = AvishkarConf()
    spark_dao = SparkDao(avishkar_conf, sc)
    mrna_features_rdd = spark_dao.get_mrna_features(args.assembly)
    candidate_set_rdd = sc.textFile(args.candidate_set_path)
    candidate_set_rdd =
    """


if __name__ == '__main__':
    sys.exit(main())
