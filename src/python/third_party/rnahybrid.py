# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import subprocess
import os
from tempfile import NamedTemporaryFile


class RNAHybrid:
    def __init__(self, avishkar_conf):
        self._executable = avishkar_conf.rnahybrid_executable()

    def __call__(self, mrna_seq, mirna_seq,
                 energy_threshold=None, helix_constraint=None):

        # TODO: Change the dataset type (3utr_human). But do we need to change
        # if we are not interested in the p-value?
        args = "-c -s 3utr_human -m %d" % len(mrna_seq)
        if helix_constraint is not None:
            args += " -f %d,%d" % (helix_constraint[0], helix_constraint[1])
        if energy_threshold is not None:
            args += " -e %0.3f" % energy_threshold

        if os.name == 'nt':
            # We are running on windows, so use files.
            fp = NamedTemporaryFile(mode='w', delete=True)
            fp.write(">mrna\n")
            fp.write("%s\n" % mrna_seq)
            fp.flush()
            cmd = "%s %s -t %s %s" % (self._executable, args,
                                      fp.name, mirna_seq)
            output = subprocess.check_output(cmd, shell=True)
            fp.close()
        else:
            cmd = "%s %s %s %s" % (self._executable, args, mrna_seq, mirna_seq)
            output = subprocess.check_output(cmd, shell=True)

        return output
