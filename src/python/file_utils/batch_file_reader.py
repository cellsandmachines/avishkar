# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys


class BatchFileReader:
    def __init__(self, fname, batch_size=sys.maxsize, process_row=lambda x: x,
                 verbose=False):
        '''
            @fname: filename
            @batch_size: maximum number of lines to read in a batch
            @process_row: function to call on each row before adding to list
        '''
        self._itr = None
        self._fname = fname
        self._batch_size = batch_size
        self._total_count = 0
        self._verbose = verbose
        self._row_func = process_row

    def close(self):
        if self._itr:
            self.close_iterator()

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, typ, value, traceback):
        self.close()

    def reset_iterator(self):
        self._itr = open(self._fname, 'rU')
        self._total_count = 0

    def close_iterator(self):
        self._itr.close()
        self._itr = None

    def read(self, data, append=False):
        if data is None or type(data) != list:
            raise Exception('Must pass a valid list to read data into')

        count = 0
        lines_remaining = False
        if not append:
            del data[:]
        if not self._itr:
            self.reset_iterator()

        for line in self._itr:
            data.append(self._row_func(line))
            count += 1
            if count >= self._batch_size:
                lines_remaining = True
                break

        if not lines_remaining:
            self.close_iterator()

        self._total_count += count
        if self._verbose:
            print "Read %d lines from file" % self._total_count
        return lines_remaining
