# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import functools
import numpy as np
from spark.clustered_svm import ClusterSvmModel
from pyspark import StorageLevel
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import SVMWithSGD
from feature_transformation.feature_transformers import (
    NanTransformer, NanMeanTransformer,
    StandardScaler, FeatureTransformer)
from models.record import Record
from metrics import Metrics


SITE_REGION_TYPES = ['3UTR', '5UTR', 'CDS']

def assign_to_cluster(x, cluster_assignment_map=None):
    """
    Cluster by microRNA family
    """
    mirna_name = x.rid()[1]
    return cluster_assignment_map.get(mirna_name, -1)


def get_features(avishkar_context,
                 data,
                 curve_means=None,
                 infer_test_mean=False):

    conf = avishkar_context.conf
    nan_transformer = NanTransformer()
    if curve_means is not None:
        # Use the mean to replace NaN
        print "Using means to replace NaN"
        print "infer_test_mean is:", infer_test_mean
        nan_transformer = NanMeanTransformer(curve_means,
                                             infer_test_mean=infer_test_mean)

    data = data.map(lambda x: nan_transformer.transform_train(x))

    feature_transformer = FeatureTransformer(
        categories=SITE_REGION_TYPES,
        basis=conf.model_param_nbasis()
    )

    # This returns (label, (numeric_feature, categorical_features))
    data = data\
        .map(
            lambda x: Record(
                feature_transformer.transform(x),
                label=x.label(),
                record_id=x.rid()
            )
        )

    numeric_features = data.map(lambda x: x[0])
    scaler = StandardScaler().fit(numeric_features)

    data = data.map(
        lambda x: Record(
            np.hstack((scaler.transform(x[0]), x[1])),
            label=x.label(),
            record_id=x.rid()
        )
    )
    return data


def train_svm(avishkar_context, train_data, cluster_assignment=None):
    metrics = Metrics(avishkar_context)
    conf = avishkar_context.conf
    kernel = conf.model_param_kernel()
    if kernel == 'LINEAR':
        # Spark expects data to be as LabeledPoint so convert to LabeledPoint
        if cluster_assignment is not None:
            # Extract data points that belong to one of the clusters.
            cluster_assignment_func = functools.partial(
                assign_to_cluster, cluster_assignment_map=cluster_assignment
            )
            train_data = train_data.filter(
                lambda x: cluster_assignment_func(x) >= 0
            )
            train_data = train_data.repartition(conf.num_partitions())

        train_data_labeled = train_data.map(
            lambda x: LabeledPoint(x.label(), x._feature_list)
        )
        model = SVMWithSGD.train(train_data_labeled,
                                 iterations=conf.training_linear_iterations(),
                                 regParam=conf.model_param_reg_param(),
                                 intercept=True)

        labels_preds = train_data.map(
            lambda x: (x, model.predict(x._feature_list))
        )

    elif kernel == 'RBF':
        num_clusters = len(set(cluster_assignment.values()))
        cluster_assignment_func = functools.partial(
            assign_to_cluster, cluster_assignment_map=cluster_assignment
        )
        model = ClusterSvmModel(num_clusters, cluster_assignment_func,
                                C=conf.model_param_svm_error_param(),
                                prob=conf.model_param_prob(),
                                gamma=conf.model_param_kernel_gamma())

        model.train(avishkar_context, train_data, use_threads=True)

        labels_preds = model.predict(train_data)

    metrics.fit(labels_preds)
    # Print training metrics
    print "Training error:", metrics.misclassification_error()
    return model


def get_logistic_prob(features, weights=None, intercept=0.0):
    prob = 1.0 / (1 + np.exp(-(features.dot(weights) + intercept)))
    return prob


def predict_svm(avishkar_context, model, test_data,
                cluster_assignment=None):

    conf = avishkar_context.conf
    kernel = conf.model_param_kernel()

    if kernel == 'LINEAR':
        if cluster_assignment is not None:
            # Extract data points that belong to one of the clusters.
            cluster_assignment_func = functools.partial(
                assign_to_cluster, cluster_assignment_map=cluster_assignment
            )
            test_data = test_data.filter(
                lambda x: cluster_assignment_func(x) >= 0
            )
            test_data = test_data.repartition(conf.num_partitions())
        labels_preds = test_data.map(
            lambda x: (x, model.predict(x._feature_list))
        ).cache()

    elif kernel == 'RBF':
        labels_preds = model.predict(test_data).cache()

    if not conf.model_param_prob():
        return labels_preds

    if kernel == 'LINEAR':
        print "Computing probability scores."
        # Compute probabilities using the weights and logistic function
        compute_prob = functools.partial(get_logistic_prob,
                                         weights=model.weights,
                                         intercept=model.intercept)
        labels_and_prob = test_data.map(
            lambda x: (x, compute_prob(x._feature_list))
        )
    else:
        # For RBF Kernel. Compute the probabilities using the model.
        labels_and_prob = model.predict_proba(test_data)

    return labels_and_prob


def train(avishkar_context, train_data,
          cluster_assignment=None,
          curve_means=None):
    train_data = get_features(
        avishkar_context, train_data,
        curve_means=curve_means)

    train_data = train_data.persist(StorageLevel.MEMORY_ONLY)

    model = train_svm(avishkar_context,
                      train_data,
                      cluster_assignment=cluster_assignment)

    train_data.unpersist()
    return model


def predict(avishkar_context, model, test_data,
            cluster_assignment=None,
            curve_means=None,
            infer_test_mean=False):

    test_data = get_features(
        avishkar_context, test_data,
        curve_means=curve_means,
        infer_test_mean=infer_test_mean)

    labels_preds = predict_svm(avishkar_context, model, test_data,
                               cluster_assignment=cluster_assignment)
    return labels_preds