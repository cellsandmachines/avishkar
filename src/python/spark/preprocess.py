# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np
from functools import partial


EMPTY_SET = set([])


def array_to_str(arr, delimiter=","):
    return delimiter.join([str(x) for x in arr])


def str_to_array(str_arr, delimiter=" ", conv_func=float):
    return np.array([conv_func(x) for x in str_arr.split(delimiter)])


def preprocess_feature(feature_row, seed_enrichment_map=None):
    # Convert the seed type to numeric attribute
    seed_pval = seed_enrichment_map.get(feature_row['seed_alignment'], 1.0)
    seed_enrichment = 1 - seed_pval
    feature_row['seed_enrichment'] = seed_enrichment

    if np.isnan(feature_row['consv_seed']):
        feature_row['is_seed_site'] = True

    # Convert consv to 0 if nan
    feature_row['consv_site'] = np.nan_to_num(feature_row['consv_site'])
    feature_row['consv_seed'] = np.nan_to_num(feature_row['consv_seed'])
    feature_row['consv_offseed'] = np.nan_to_num(feature_row['consv_offseed'])
    return feature_row


def filter_rows(row, exclude_seed_sites=False,
                exclude_seedless_sites=False,
                exclude_region=EMPTY_SET):
    """
    Filter, i.e. emit, features based on specified criteria. By default filters
    everything.
    :param row: A feature Record.
    :param exclude_seed_sites: If True then don't emit seed sites.
    :param exclude_seedless_sites: : If True then dont emit seedless sites.
    :param exclude_region: If not None, then exclude target sites present in the specified region.
    :return: True if the feature is to be filtered i.e. emitted. Returns False if the
     feature record is to be excluded.
    """
    if exclude_seed_sites and row['is_seed_site']:
        return False
    if exclude_seedless_sites and not row['is_seed_site']:
        return False
    if row['site_region'] in exclude_region:
        return False
    return True


def preprocess_data(feature_rdd,
                    exclude_seed_sites=False,
                    exclude_seedless_sites=False,
                    exclude_region=EMPTY_SET,
                    seed_enrichment_map=None):

    preprocess_func = partial(preprocess_feature, seed_enrichment_map=seed_enrichment_map)
    filter_rows_func = partial(filter_rows, exclude_seed_sites=exclude_seed_sites,
                               exclude_seedless_sites=exclude_seedless_sites,
                               exclude_region=exclude_region)

    feature_rdd = feature_rdd.map(preprocess_func)
    feature_rdd = feature_rdd.filter(lambda x: filter_rows_func(x))
    return feature_rdd
