# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__ = 'asish'
import numpy as np
from sklearn.metrics import roc_curve


def update_confusion(label, pred, confusion_matrix):
    label, pred = int(label), int(pred)
    if label == 1:
        if label == pred:
            confusion_matrix[0].add(1)
        else:
            confusion_matrix[1].add(1)
    else:
        if label == pred:
            confusion_matrix[3].add(1)
        else:
            confusion_matrix[2].add(1)


def get_confusion_matrix(sc, record_rdd):
    confusion_matrix = []
    for i in range(4):
        confusion_matrix.append(sc.accumulator(i))

    record_rdd.foreach(lambda (x, p): update_confusion(x.label(), p, confusion_matrix))
    confusion_matrix = [[confusion_matrix[0].value, confusion_matrix[1].value],
                        [confusion_matrix[2].value, confusion_matrix[3].value]]
    confusion_matrix = np.array(confusion_matrix)
    confusion_matrix = confusion_matrix.astype(float)
    return confusion_matrix

def get_partition_fpr_tpr(records):
    thresholds = np.arange(0, 1, 0.005)
    # Keeps counts of tp, fp, tn and fn for each threshold
    counts = np.zeros((thresholds.size, 4))
    for (rec, prob) in records:
        preds = (prob >= thresholds).astype(int)
        label = int(rec.label())
        matches = np.where(label == preds)
        mismatches = np.where(label != preds)
        if label == 1:
            # Increment the TPs
            counts[matches, 0] += 1
            # Increment the FNs
            counts[mismatches, 3] += 1
        else:
            # Increment the TNs
            counts[matches, 2] += 1
            # Increment the FPs
            counts[mismatches, 1] += 1

    for i in range(thresholds.size):
        yield (str(thresholds[i]), counts[i, :])


def get_threshold_scores(records_rdd):
    th_scores = records_rdd\
        .mapPartitions(get_partition_fpr_tpr, preservesPartitioning=True)\
        .reduceByKey(lambda scores1, scores2: scores1 + scores2).collect()

    return dict(th_scores)


class Metrics:
    def __init__(self, avishkar_context):
        self._context = avishkar_context
        self._conf_matrix = np.zeros((2, 2))
        self._threshold_scores = {}

    def __add__(self, other):
        new_metrics = Metrics(self._context)
        new_metrics._conf_matrix = self.confusion_matrix() + other.confusion_matrix()
        for k in self._threshold_scores.keys():
            new_metrics._threshold_scores[k] = self._threshold_scores[k] + other._threshold_scores[k]
        return new_metrics

    def fit(self, record_rdd):
        sc = self._context.sc
        if not self._context.conf.model_param_prob():
            self._conf_matrix = get_confusion_matrix(sc, record_rdd)
            tp = float(self._conf_matrix[0, 0])
            tn = float(self._conf_matrix[1, 1])
            fp = float(self._conf_matrix[1, 0])
            fn = float(self._conf_matrix[0, 1])
            self._threshold_scores["0.5"] = (tp, fp, tn, fn)

        else:
            self._conf_matrix = get_confusion_matrix(
                sc,record_rdd.map(lambda (r, prob): (r, int(prob >= 0.5))))
            self._threshold_scores = get_threshold_scores(record_rdd)


    def confusion_matrix(self):
        return self._conf_matrix

    def misclassification_error(self):
        misclassified = self._conf_matrix[0, 1] + self._conf_matrix[1, 0]
        total = np.sum(self._conf_matrix)
        return misclassified/float(total)

    def get_precision_recall(self):
        tp = float(self._conf_matrix[0, 0])
        fp = float(self._conf_matrix[1, 0])
        fn = float(self._conf_matrix[0, 1])
        precision = (tp/(tp + fp))
        recall = (tp/(tp + fn))
        return (precision, recall)

    def get_fpr_tpr(self):
        thresholds = list(enumerate(sorted(map(float, self._threshold_scores.keys()),
                                           reverse=True)))
        counts = np.zeros((len(self._threshold_scores), 4))
        for i, th in thresholds:
            counts[i, :] = self._threshold_scores[str(th)]
        fpr = counts[:, 1]/(counts[:, 1] + counts[:, 2])
        tpr = counts[:, 0]/(counts[:, 0] + counts[:, 3])
        return (fpr, tpr, [x[1] for x in thresholds])