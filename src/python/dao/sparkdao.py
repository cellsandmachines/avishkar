# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
    DAO interface to access data using Spark SQL
"""
from config.avishkarconf import AvishkarConf
from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
from models.rna import MessengerRNA, MicroRNA
import numpy as np


def get_row_rdd(rdd, col_names):
    return rdd.map(lambda x: Row(**dict(zip(col_names, x.split(",")))))


def quote_strings(str_list):
    return map(lambda x: '\'%s\'' % x, str_list)


class SparkDao:
    def __init__(self, avishkar_conf, spark_context):
        self._sc = spark_context
        self.sql_context = SQLContext(self._sc)
        self.partition_func = lambda x: hash(x)
        self.default_partitions = avishkar_conf.num_partitions()
        self.seed_enrichment_cache = {}

        # Rgister all files as tables.
        # Each section describes a file
        for table_name in avishkar_conf.sparkdao_tables():
            hdfs_path, col_names =\
                avishkar_conf.sparkdao_table_details(table_name)
            print "Registering %s as table: %s" % (hdfs_path, table_name)

            rdd = self._sc.textFile(hdfs_path)
            row_rdd = get_row_rdd(rdd, col_names)
            schema = self.sql_context.inferSchema(row_rdd)
            schema.registerTempTable(table_name)

    def get_mrna_sequences(self, assembly,
                           mrna_filter_func=None,
                           num_partitions=None):
        """
            Returns RDD of @MessengerRNA objects
            assembly specifies the assembly to be used.
            The table name is generated from assembly as assembly_MRNA_SEQ
            @filter_func: Filter mRNA by name.
        """
        table_name = "%s_MRNA_SEQ" % assembly.upper()
        print "Retrieving data from table:", table_name
        result = self.sql_context.sql('select * from %s' % table_name)
        result = result.map(lambda r: (r.MRNA_NAME,
                                       MessengerRNA(r.MRNA_NAME, r.SEQ)))

        if mrna_filter_func is not None:
            result = result.filter(lambda (k, _): mrna_filter_func(k))

        num_partitions = self.default_partitions if num_partitions is None\
            else num_partitions
        partition_func = self.partition_func
        result = result.partitionBy(num_partitions, partition_func)
        return result

    def get_mirna_sequences(self, mirna_filter_func=None):
        """
            Returns RDD of @MicroRNA objects
        """
        query = 'select * from MIRNA_SEQ'
        result = self.sql_context.sql(query)
        result = result.map(lambda r: (r.MIRNA_NAME,
                                       MicroRNA(r.MIRNA_NAME, r.SEQ)))

        if mirna_filter_func is not None:
            result = result.filter(lambda (k, _): mirna_filter_func(k))

        return result

    def get_mrna_features(self, assembly, mrna_filter_func=None,
                          num_partitions=None):
        """
            Returns the mRNA features:
                mrna_name, (5' UTR start, 5' UTR end),
                (3' UTR start, 3' UTR end), (CDS start, CDS end),
                consv_seq
        """
        mrna_table = '%s_MRNA_FEATURES' % assembly.upper()
        result = self.sql_context.sql(
            'select * from %s' % (mrna_table)
        )
        result = result.map(
            lambda x: (x.MRNA_NAME,
                       [(int(x.UTR5_START), int(x.UTR5_END)),
                        (int(x.UTR3_START), int(x.UTR3_END)),
                        (int(x.CDS_START), int(x.CDS_END)),
                        np.array([float(y) for y in x.CONSV_SEQ.split(" ")])])
        )
        if mrna_filter_func is not None:
            result = result.filter(lambda (k, _): mrna_filter_func(k))

        num_partitions = self.default_partitions if num_partitions is None\
            else num_partitions
        partition_func = self.partition_func
        result = result.partitionBy(num_partitions, partition_func)
        return result

    def get_clip_locations(self, dataset_name, mrna_filter_func=None,
                           num_partitions=None):
        '''
            Returns RDD of mRNA name, List<CLIP locations>
        '''
        results = self.sql_context.sql('select * from %s' % dataset_name)
        results = results.map(
            lambda x: (x.MRNA_NAME, (int(x.START), int(x.END)))
        )

        if mrna_filter_func is not None:
            results = results.filter(lambda (k, _): mrna_filter_func(k))

        results = results.groupByKey().map(lambda (k, v_itr): (k, set(v_itr)))
        num_partitions = self.default_partitions if num_partitions is None\
            else num_partitions
        partition_func = self.partition_func
        results = results.partitionBy(num_partitions, partition_func)
        return results

    def get_mrna_clip_data(self, assembly, dataset_name,
                           num_partitions=None,
                           mrna_filter_func=None):
        '''
            Returns tuples of (mRNA name, (@MessengerRNA, List<CLIP locations>))
            where a CLIP Location is in turn a tuple.
        '''
        mrna_rdd = self.get_mrna_sequences(assembly,
                                           mrna_filter_func=mrna_filter_func)
        clip_rdd = self.get_clip_locations(dataset_name,
                                           mrna_filter_func=mrna_filter_func)

        num_partitions = self.default_partitions if num_partitions is None\
            else num_partitions
        partition_func = self.partition_func

        result_rdd = mrna_rdd\
            .join(clip_rdd)\
            .map(lambda (k, (m, l)): (k, (m, l)))\
            .partitionBy(num_partitions, partition_func)

        return result_rdd

    def get_seed_enrichment_map(self, dataset_name, cache=True):
        """
            Returns a map where the key is the
            seed alignment type and the value is the enrichment score.
        """
        if cache is True and dataset_name in self.seed_enrichment_cache:
            return self.seed_enrichment_cache[dataset_name]

        print "Reading enrichment scores from table:", dataset_name
        rdd = self.sql_context.sql(
            "select SEED_ALIGNMENT_TYPE, ENRICHMENT_SCORE from %s" % dataset_name
        )
        enrichment_map = dict(rdd.map(lambda (k, v): (k, float(v))).collect())
        if cache is True:
            self.seed_enrichment_cache[dataset_name] = enrichment_map
        return enrichment_map


if __name__ == '__main__':
    sc = SparkContext(appName="SparkDao Test")
    avishkar_conf = AvishkarConf()
    dao = SparkDao(avishkar_conf, sc)

    for mirna in dao.get_mirna_sequences(
            mirna_names=['hsa-let-7a-5p', 'hsa-let-7a-3p']).collect():
        print mirna
