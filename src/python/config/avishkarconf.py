# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from ConfigParser import SafeConfigParser
import os
import os.path
import random


# Environment variables needed by the program.
ENV_VARIABLES = ['AVISHKAR_HOME']


class AvishkarConf:
    def __init__(self, overrides=None):
        """
            @overrides: A dictionary of key values that overrides those
            from the config files.
            Key should be of type: <section_name>:<config_key>
            e.g. MODEL-PARAMS:reg_param
        """
        # Only take the required environment variables from os.environ
        dict_type = dict(map(lambda k: (k, os.environ[k]), ENV_VARIABLES))
        self._main_parser = SafeConfigParser(dict_type)
        self._main_parser.read(os.path.join(os.environ['AVISHKAR_HOME'],
                                            'conf/avishkar.conf'))
        self._sparkdao_parser = SafeConfigParser(dict_type)
        self._sparkdao_parser.read(os.path.join(os.environ['AVISHKAR_HOME'],
                                                'conf/sparkdao.conf'))

        if overrides is not None:
            for (k, v) in overrides.iteritems():
                (section, key) = k.split(":")
                self._main_parser.set(section, key, v)

    def rnahybrid_executable(self):
        """ Returns path of the RNAhybird executable """
        return self._main_parser.get('THIRD-PARTY', 'rna_hybrid')

    def rnafold_executable(self):
        """ Returns path of the RNAfold executable """
        return self._main_parser.get('THIRD-PARTY', 'rna_fold')

    def sparkdao_table_details(self, table_name):
        """ Given the table name returns the path and column names """
        if not self._sparkdao_parser.has_section(table_name):
            raise Exception('No configuration exists for table %s' % table_name)
        hdfs_path = self._sparkdao_parser.get(table_name, 'hdfs_path')
        col_names = self._sparkdao_parser.get(table_name, 'col_names')\
            .split(',')
        return (hdfs_path, col_names)

    def sparkdao_tables(self):
        """ Returns all tables defined in the config file """
        return self._sparkdao_parser.sections()

    def set_model_param(self, key, value):
        """
        Sets the value for a particular model parameter.
        :param key: Can be one of the following:
            model_type => String. Values: FN, MV, FN_PCA
            nbasis => Int and < 27. Number of basis functions to use.
            svm_error_param => Double. Error parameter (C) for Kernel SVM
            reg_param => Double. Regularization param for linear model.
            kernel => String. Values: LINEAR, RBF
            gamma => Double. Paramter of RBF kernel.
            compute_prob => String. Values: True, False

        :param value: See above.
        :return: None.
        """
        self._main_parser.set("MODEL-PARAMS", key, value)

    def model_param_model_type(self):
        return self._main_parser.get("MODEL-PARAMS", "model_type")

    def model_param_nbasis(self):
        return self._main_parser.getint("MODEL-PARAMS", "nbasis")

    def model_param_svm_error_param(self):
        return self._main_parser.getfloat("MODEL-PARAMS", "svm_error_param")

    def model_param_reg_param(self):
        return self._main_parser.getfloat("MODEL-PARAMS", "reg_param")

    def model_param_kernel(self):
        return self._main_parser.get("MODEL-PARAMS", "kernel")

    def model_param_kernel_gamma(self):
        return self._main_parser.getfloat("MODEL-PARAMS", "gamma")

    def model_param_prob(self):
        return self._main_parser.getboolean("MODEL-PARAMS", "compute_prob")

    def crossvalidation_folds(self):
        return self._main_parser.getint("CROSSVALIDATION", "folds")

    def sampling_subsample_runs(self):
        return self._main_parser.getint("SAMPLING", "subsample_runs")

    def sampling_sample_frac(self):
        return self._main_parser.getfloat("SAMPLING", "sample_frac")

    def tune_nbasis(self):
        return self._main_parser.getint("TUNE", "nbasis")

    def tune_kernel(self):
        return self._main_parser.get("TUNE", "kernel")

    def tune_svm_error_param(self):
        return self._main_parser.getfloat("TUNE", "svm_error_param")

    def tune_reg_param(self):
        return self._main_parser.getfloat("TUNE", "reg_param")

    def tune_gamma(self):
        return self._main_parser.getfloat("TUNE", "gamma")

    def num_partitions(self):
        return self._main_parser.getint("PARALLELIZATION", "num_partitions")

    def set_num_partitions(self, partitions):
        return self._main_parser.set("PARALLELIZATION", "num_partitions", str(partitions))

    def get_random_seed(self):
        seed = self._main_parser.getint("OTHERS", "random_seed")
        if type(seed) == str:
            if seed == "random":
                return random.randint(1, 100)
            else:
                raise Exception("random_seed should be either an integer"
                                " or the string random")
        else:
            return int(seed)

    def training_linear_iterations(self):
        return self._main_parser.getint("TRAINING", "linear_iterations")

