# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np
from smoothing import smooth_spline


def set_index(index, arr_size):
    arr = np.zeros(arr_size)
    arr[index] = 1
    return arr


class NanTransformer:
    def transform_train(self, features):
        keys = ['delg_site', 'delg_seed', 'deldelg_site', 'deldelg_seed',
                'au_site', 'au_seed']
        for key in keys:
            features[key] = np.nan_to_num(features[key])
        return features

    def transform_test(self, x):
        return self.transform_train(x)

# TODO: Fix this. Replace feature indices by names.
class NanMeanTransformer(NanTransformer):
    def __init__(self, means, infer_test_mean=False):
        # @means is an array of tuples where
        # arr[i] = (+ve mean for ith feature, -ve mean for ith feature)
        self._means = means
        if infer_test_mean is False:
            self.transform_test = self.transform_train

    def _replace_nan(self, x, mean_arr):
        nan_indices = np.where(np.isnan(x))
        x[nan_indices] = mean_arr[nan_indices]
        return x

    def transform_train(self, features):
        label = features.label()
        for i in range(3):
            features[i] = map(lambda (j, f):
                              self._replace_nan(
                                  f, self._means[2*i + j][int(label)]),
                              enumerate(features[i]))

        return features

    def _diff(self, x, mean_arr):
        non_nan_ind = np.where(~ np.isnan(x))[0]
        diff = x[non_nan_ind] - mean_arr[non_nan_ind]
        return diff.dot(diff)

    def _infer_label(self, x, means_arr):
        diff_neg = self._diff(x, means_arr[0])
        diff_pos = self._diff(x, means_arr[1])
        return 1 if diff_pos <= diff_neg else 0

    def transform_test(self, features):
        # Infer label based on DelG site
        label = self._infer_label(features[0][0], self._means[0])
        for i in range(3):
            features[i] = map(lambda (j, f):
                              self._replace_nan(f, self._means[2*i + j][label]),
                              enumerate(features[i]))

        return features


class StandardScaler:
        def __init__(self):
                self._mean = 0
                self._std = 1

        def fit(self, rdd):
                ssum, sum_sq, scount = rdd.map(lambda x: [x, x**2, 1])\
                    .reduce(lambda x, y: [i + j for (i, j) in zip(x, y)])
                self._mean = ssum/float(scount)
                self._std = np.sqrt(sum_sq/float(scount) - (self._mean ** 2))
                return self

        def transform(self, data):
                return (data - self._mean)/self._std


class FeatureTransformer:
    '''
        Transforms multivariate features to functions features.
        Transform categorical features to one hot encoding.
    '''
    def __init__(self, categories=[], **model_params):
        self._model_params = model_params
        self._category_map = {}
        for i, v in enumerate(categories):
            self._category_map[v] = i
        self._num_categories = len(categories)

    def _get_other_features(self, x):
        return [x[i] for i in ('consv_site', 'consv_seed', 'consv_offseed',
                               'rel_site_loc', 'seed_enrichment', 'site_len')]

    def transform(self, x):
        delg_site, delg_seed = x['delg_site'], x['delg_seed']
        deldelg_site, deldelg_seed = x['deldelg_site'], x['deldelg_seed']
        au_site, au_seed = x['au_site'], x['au_seed']
        fn_features = [delg_site, delg_seed,
                       deldelg_site, deldelg_seed,
                       au_site, au_seed]
        site_region = x['site_region']
        other_features = self._get_other_features(x)

        # smooth functional data if needed
        if self._model_params['basis'] > 0:
            for i in range(len(fn_features)):
                fn_features[i] = smooth_spline(
                    fn_features[i], nbasis=self._model_params['basis']
                ).get_coeffs()

        fn_features = np.hstack(fn_features)

        site_region = set_index(self._category_map[site_region],
                                self._num_categories)

        numeric_features = np.hstack((fn_features,
                                      other_features
                                      ))

        return (numeric_features, site_region)
