# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from scipy.interpolate import LSQUnivariateSpline
import numpy as np


def smooth_spline(y, nbasis=20):
    x = np.linspace(0, 1, y.size)
    norder = 4  # For cubic spline
    nbreaks = nbasis - norder + 2
    breaks = np.linspace(x[0], x[-1], nbreaks)
    breaks = breaks[1:-1]  # Use only the interior points
    spl = LSQUnivariateSpline(x, y, breaks)
    return spl
