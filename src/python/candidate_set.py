# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


'''
    From clip tag locations in mrna. Generate data for mrna-mirna pair.
'''
import sys
from third_party.rnahybrid import RNAHybrid
import argparse
from pyspark import SparkContext
from dao.sparkdao import SparkDao
from avishkar_context import AvishkarContext
from models.target_location import LocationWithAttribute


def parse_arguments():
    argparser = argparse.ArgumentParser(
        description="Generate mRNA-miRNA data from CLIP data which is"
        " later used for training and validation. "
    )
    argparser.add_argument(
        "assembly", choices=['mm9', 'hg19', 'hg18'],
        help="The sequence assembly to use for mRNA."
    )
    argparser.add_argument(
        "clip_dataset",
        help="The CLIP dataset to use. This should correspond to "
        "the filename in HDFS that stores the CLIP locations."
    )
    argparser.add_argument(
        "output_file",
        help="File to write the output to."
    )
    argparser.add_argument(
        "--mirna-regex", default=None, dest='mirna_regex',
        help="Regular experssion to use for miRNA names to filter miRNA. "
    )
    argparser.add_argument(
        "--mirna-names", nargs='*', default=[], dest='mirna_names',
        help="miRNA names to filter."
    )
    args = argparser.parse_args()
    return args


def get_match_len_and_type(mrna_mismatch, mrna_match,
                           mirna_mismatch, mirna_match):
    # Returns the length of mrna that is actually aligned (excluding gaps)
    # Returns the start and end position of mirna match from the 5' end until
    # nucleotide position 8.
    # Note: The returned indices are 1 based and matches are perfect (i.e.
    # no GU wobbles)

    max_align_len = [mrna_mismatch, mrna_match, mirna_mismatch, mirna_match]
    max_align_len = max(map(lambda x: len(x), max_align_len))
    mrna_mismatch += " " * (max_align_len - len(mrna_mismatch))
    mrna_match += " " * (max_align_len - len(mrna_match))
    mirna_mismatch += " " * (max_align_len - len(mirna_mismatch))
    mirna_match += " " * (max_align_len - len(mirna_match))

    mrna_align_len = 0
    for i in range(max_align_len):
        # If both mismatch and match are not gaps then increment
        # the mrna align length.
        if mrna_mismatch[i] == " " and mrna_match[i] == " ":
            continue
        mrna_align_len += 1

    mirna_start = 0
    mirna_start_offset = 0  # accounts for gaps at the beginning of miRNA.
    mirna_end = 0
    # We are looking for a block of matches from the 5' end of the miRNA
    # without any GU wobbles.
    # Ignore GU wobbles at the beginning
    for i in range(1, max_align_len):
        if (mirna_mismatch[-i] == " " and mirna_match[-i] == " "):
            mirna_start_offset += 1
            continue
        break

    for i in range(1 + mirna_start_offset, max_align_len):
        # If we have a Gap or mismatch or a GU wobble then increment the start
        if mirna_match[-i] == " " or\
                (mirna_match[-i] == "G" and mrna_match[-i] == "U") or\
                (mirna_match[-i] == "U" and mrna_match[-i] == "G"):
            mirna_start += 1
        # The moment we have a match break.
        if mirna_match[-i] != " ":
            break

    mirna_start += 1

    mirna_end = mirna_start
    for i in range(mirna_start + mirna_start_offset, 9 + mirna_start_offset):
        if mirna_match[-i] == " " or\
                (mirna_match[-i] == "G" and mrna_match[-i] == "U") or\
                (mirna_match[-i] == "U" and mrna_match[-i] == "G"):
            break
        mirna_end += 1

    mirna_end -= 1

    return (mrna_align_len, (mirna_start, mirna_end))


def contains(loc, other_loc):
    ''' Returns True if @loc is contained within @other_loc '''
    if loc[0] >= other_loc[0] and loc[1] <= other_loc[1]:
        return True
    return False


def parse_rna_hybrid_output(outputs, enforce_seed_match=False):
    # Parse output to get the following features:
    #   Mean free energy (mfe) of duplex.
    #   Start and end position of match
    #   GU wobbles are not considered.
    loc_attr_list = []
    outputs = outputs.split('\n')
    for output in outputs:
        output = output.strip()
        output = output.split(":")
        if not output or not output[0]:
            continue
        mfe = float(output[4])
        if not mfe < 0.0:
            continue
        mrna_mismatch, mrna_match, mirna_match, mirna_mismatch =\
            output[7], output[8], output[9], output[10]
        mrna_align_len, seed_type = get_match_len_and_type(
            mrna_mismatch, mrna_match, mirna_mismatch, mirna_match
        )

        start = int(output[6])
        end = start + mrna_align_len

        if not enforce_seed_match:
            # We have no seed match constraints
            loc_attr_list.append(
                LocationWithAttribute((start, end), mfe, None)
            )
        else:
            if seed_type[1] - seed_type[0] + 1 >= 6:
                loc_attr_list.append(
                    LocationWithAttribute((start, end), mfe, seed_type)
                )

    return loc_attr_list


def resolve_ovarlapping_locations(loc_attr_list):
    # Resolved LocationWithAttribute objects
    resolved_loc_attr = {}
    for loc_attr in loc_attr_list:
        if loc_attr in resolved_loc_attr and \
                resolved_loc_attr[loc_attr].is_preferable(loc_attr):
                continue
        resolved_loc_attr[loc_attr] = loc_attr

    return sorted(resolved_loc_attr.values(), key=lambda x: x._loc)


def get_labeled_target_locations(rna_hybrid,
                                 micro_rna, mrna, clip_locations):
    '''
        Gets potential target locations in @mrna due to @micro_rna
        If the locations overlap/is contained in a clip location
        in clip_locations then
        return 1 for that location otherwise return 0 for that location
    '''
    # TODO: Move this to config
    output = rna_hybrid(mrna._seq, micro_rna._seq, energy_threshold=-15.0)
    loc_attrs = parse_rna_hybrid_output(output)
    constraints = [(1, 6), (2, 7), (3, 8)]
    for cons in constraints:
        output = rna_hybrid(mrna._seq, micro_rna._seq,
                            energy_threshold=-1.0,
                            helix_constraint=cons)
        loc_attrs += parse_rna_hybrid_output(output, enforce_seed_match=True)
    loc_attrs = resolve_ovarlapping_locations(loc_attrs)

    if loc_attrs is None:
        return None

    candidate_set = []
    for loc_attr in loc_attrs:
        label = None
        if clip_locations is not None:
            label = 0
            for other_loc in clip_locations:
                if contains(loc_attr._loc, other_loc):
                    label = 1
                    break
        candidate_set.append((label, loc_attr))

    print "%s, %s: locations: %d" % (mrna._name, micro_rna._name,
                                     len(candidate_set))
    return (mrna, micro_rna, candidate_set)


def process_partition(conf, itr):
    rna_hybrid = RNAHybrid(conf)
    for (_, x) in itr:
        mrna, mirna, clip_locations = x[0], x[1], x[2]
        yield get_labeled_target_locations(rna_hybrid,
                                           mirna, mrna, clip_locations)


class CandidateSetGenerator:
    def __init__(self, avishkar_context):
        """
            Generate candidate set for each mRNA-miRNA pair.
            Returns (@MessengerRNA, @MicroRNA, List<LabelledLocation>)
            where LabelledLocation is a tuple of:
                (loc, label, mfe, seed match type)
        """
        self._context = avishkar_context
        self._num_partitions = avishkar_context.dao.default_partitions
        self._partition_func = avishkar_context.dao.partition_func

    def generate(self, assembly,
                 clip_dataset=None,
                 mrna_filter_func=None,
                 mirna_filter_func=None):

        # TODO: Repartition the data for even distribution across nodes?
        # TODO: Partition data such that the total length of transcript
        # on each node remains approximately the same.

        mirna_rdd = self._context.dao\
            .get_mirna_sequences(mirna_filter_func=mirna_filter_func)
        mirnas = self._context.sc.broadcast(mirna_rdd.collect())

        if clip_dataset is not None:
            mrna_clip_rdd = self._context.dao\
                .get_mrna_clip_data(assembly, clip_dataset,
                                    mrna_filter_func=mrna_filter_func)

            combined_rdd = mrna_clip_rdd\
                .flatMap(
                    lambda (k, m): map(lambda (_, mi): (k, m, mi), mirnas.value)
                )\
                .map(lambda (k, m, mi): (k, (m[0], mi, m[1])))
        else:
            mrna_seq_rdd = self._context.dao.get_mrna_sequences(assembly,
                                                                mrna_filter_func=mrna_filter_func)
            combined_rdd = mrna_seq_rdd\
                .flatMap(
                    lambda (k, m): map(lambda (_, mi): (k, m, mi), mirnas.value)
                )\
                .map(lambda (k, m, mi): (m, (m, mi, None)))

        combined_rdd = combined_rdd.partitionBy(self._num_partitions,
                                                self._partition_func)

        conf = self._context.conf
        return combined_rdd.mapPartitions(
            lambda itr: process_partition(conf, itr),
            preservesPartitioning=True
        )

    def read_from_file(self, fname):
        rdd = self._context.sc.textFile(fname)
        rdd = rdd.map(lambda x: x.strip().split(","))

        # Join with mRNA sequences
        num_partitions = self._context.dao.default_partitions
        partition_func = self._context.dao.partition_func
        rdd = rdd.map(lambda x: (x[1], x))
        rdd = rdd.partitionBy(num_partitions, partition_func)

        mrna_filter_func = self._mrna_filter_func
        mirna_filter_func = self._mirna_filter_func
        mrna_seq_rdd = self._context.dao\
            .get_mrna_sequences(self._assembly,
                                mrna_filter_func)
        rdd = rdd.join(mrna_seq_rdd)

        # Join with miRNA sequences
        # Key: MiRNA. Value is mRNA object and row from file
        rdd = rdd.map(lambda (k, (r, m)): (r[2], (m, r)))

        mirna_seq_rdd = self._context.dao\
            .get_mirna_sequences(mirna_filter_func)

        rdd = rdd.join(mirna_seq_rdd).coalesce(num_partitions)

        # Key: (mRNA name, miRNA name). Value is mRNA obj, miRNA obj, label,
        # loc, energy and seed type
        rdd = rdd.map(lambda (k, (x, mi)):
                      ((x[1][1], x[1][2]), (x[0], mi, x[1][0], x[1][3:])))
        rdd = rdd.groupByKey().map(lambda (k, x): from_csv(x))
        return rdd

    def write_to_file(self, rdd, fname):
        rdd.flatMapValues(lambda x: to_csv(*x)).saveAsTextFile(fname)


def from_csv(itr):
    candidate_sets = []
    mrna = None
    mirna = None
    for x in itr:
        (mrna, mirna, label, (loc, energy, seed_type)) = x
        label = None if label == "None" else int(label)
        loc = tuple(map(int, loc.split("-")))
        energy = float(energy)
        seed_type = None if seed_type == "None" else\
            tuple(map(int, seed_type.split("-")))
        candidate_sets.append((label,
                               LocationWithAttribute(loc, energy, seed_type)))

    return (mrna, mirna, candidate_sets)


def to_csv(mrna, mirna, candidate_sets):
    for (label, loc_attr) in candidate_sets:
        yield "%d,%s,%s,%s" % (label, mrna._name, mirna._name, str(loc_attr))


def main():
    args = parse_arguments()
    if len(args.mirna_names) == 0:
        args.mirna_names = None
    sc = SparkContext(appName="Avishkar:: Candidate set generation")

    avishkar_context = AvishkarContext()
    spark_dao = SparkDao(avishkar_context, sc)
    mrna_clip_rdd = spark_dao.get_mrna_clip_data(
        args.assembly, args.clip_dataset
    )
    mirna_rdd = spark_dao.get_mirna_sequences(
        mirna_regex=args.mirna_regex, mirna_names=args.mirna_names
    )
    generator = CandidateSetGenerator(avishkar_context,
                                      mrna_clip_rdd, mirna_rdd)
    output_rdd = generator.generate()
    generator.write_to_file(output_rdd, args.output_file)
    return 0

if __name__ == '__main__':
    sys.exit(main())
