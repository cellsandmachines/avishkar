# Copyright 2015 Asish Ghoshal.
# 
# This file is part of Avishkar.
# 
# Avishkar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from config.avishkarconf import AvishkarConf
from dao.sparkdao import SparkDao


class AvishkarContext:
    def __init__(self, spark_context, config_overrides=None):
        """
            @spark_context: Spark Context object
            @config_overrides: A dictionary of key values that overrides those
            from the config files.
            Key should be of type: <section_name>:<config_key>
            e.g. MODEL-PARAMS:reg_param
        """
        self.sc = spark_context
        self.conf = AvishkarConf(overrides=config_overrides)
        self.dao = SparkDao(self.conf, spark_context)
